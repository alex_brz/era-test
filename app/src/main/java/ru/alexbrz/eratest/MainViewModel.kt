package ru.alexbrz.eratest

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import java.util.ArrayList

class MainViewModel: ViewModel() {
    private val _messages = mutableStateListOf(SampleData.conversationSample)

    private fun <T> mutableStateListOf(elements: ArrayList<T>): SnapshotStateList<T> {
        val list = mutableStateListOf<T>()
        elements.forEach {
            list.add(it)
        }
        return list
    }

    val messages: SnapshotStateList<Message> get() = _messages

    fun sendMessage(textValue: String){
        _messages.add(Message(textValue, true))
    }

}
package ru.alexbrz.eratest.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val Grey800 = Color(0xFF424242)
val Grey900 = Color(0xFF292929)
val Grey1000 = Color(0xFF181818)

val RedA700 = Color(0xFFD50000)
val Red600 = Color(0xFFE53935)
val Red400 = Color(0xFFef5350)
package ru.alexbrz.eratest

import android.content.res.Configuration
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Send
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import ru.alexbrz.eratest.ui.theme.EraTestTheme
import androidx.lifecycle.viewmodel.compose.viewModel
import ru.alexbrz.eratest.ui.theme.Red400
import ru.alexbrz.eratest.ui.theme.Red600


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            EraTestTheme(darkTheme = true) {
                val model: MainViewModel = viewModel()
                Scaffold(
                    topBar = {
                        TopAppBar(
                            title = { Text(text = stringResource(id = R.string.era_profile_name), color = Color.White) },
                            backgroundColor = MaterialTheme.colors.primary
                        )
                    },
                    bottomBar = {
                        BottomAppBar() {
                            var textValue by remember { mutableStateOf("") }
                            TextField(
                                value = textValue,
                                modifier = Modifier,
                                onValueChange = {
                                    textValue = it
                                },
                                singleLine = true,
                                keyboardOptions = KeyboardOptions(
                                    keyboardType = KeyboardType.Text
                                )
                            )
                            Spacer(Modifier.weight(1f, true))
                            IconButton(onClick = {
                                model.sendMessage(textValue)
                                textValue = ""
                            }) {
                                Icon(Icons.Filled.Send, contentDescription = "Localized description")
                            }
                        }
                    },
                    //backgroundColor = MaterialTheme.colors.ba
                ) {
                    Conversation(model, it.calculateBottomPadding())
                }
            }
        }
    }
}

data class Message(val body: String, val fromUser: Boolean = false)

@Composable
fun MessageCard(msg: Message) {
    // Add padding around our message
    Row(modifier = Modifier.padding(all = 8.dp).fillMaxWidth(), horizontalArrangement = if(msg.fromUser) Arrangement.End else Arrangement.Start) {
        if(!msg.fromUser) {
            Image(
                painter = painterResource(id = R.drawable.era_profile),
                contentDescription = "contact profile picture",
                modifier = Modifier
                    .size(40.dp)
                    .clip(CircleShape)
                    .border(1.5.dp, MaterialTheme.colors.secondary, CircleShape)
            )

            Spacer(modifier = Modifier.width(8.dp))
        }

        // We keep track if the message is expanded or not in this variable
        //var isExpanded by remember { mutableStateOf(false) }

        Column(
            modifier = Modifier
                .padding(all = 2.dp)
        ) {
            if(!msg.fromUser) {
                Text(
                    text = stringResource(id = R.string.era_profile_name),
                    color = Color.White,
                    style = MaterialTheme.typography.subtitle2
                )

                Spacer(modifier = Modifier.height(4.dp))
            }

            Surface(
                shape = MaterialTheme.shapes.medium,
                elevation = 1.dp,
                color = if(msg.fromUser) Red400 else MaterialTheme.colors.primary,
            ) {
                Text(
                    text = msg.body,
                    modifier = Modifier
                        .padding(all = 4.dp)
                        .animateContentSize(),
                    style = MaterialTheme.typography.body2,
                    color = Color.White
                )
            }
        }
    }
}


@Composable
fun Conversation(model: MainViewModel, bottomPadding: Dp) {
    LazyColumn(modifier = Modifier.padding(bottom = bottomPadding), ) {
        item {
            Text(model.messages.size.toString())
        }
        items(model.messages) {message ->
            MessageCard(message)
        }
    }
}


@Preview(name = "Light Mode")
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    name = "Dark Mode"
)
@Composable
fun PreviewMessageCard() {
    EraTestTheme {

        MessageCard(
            msg = Message("Hey, take a look at Jetpack Compose, it's great!", false)
        )
        MessageCard(
            msg = Message("Hey, take a look at Jetpack Compose, it's great!", true)
        )
    }
}
